import React, { useRef, useEffect } from 'react';
import { get } from 'lodash';

import data from './demo';


const SIZE = {
  height: 800,
  width: 1200
};
// 1000
const sVal = (x, y) => {
  return [x * SIZE.width / 1000, y * SIZE.height / 1000]
}


// const calVal = (x, y) => {
//   return [SIZE.width * (+x + 60000) / 120000, SIZE.height * (-(+y) + 30000) / 60000]
// }
const calVal = (x, y) => {
  return [SIZE.width * (+x ) / 60000, SIZE.height * (-(+y)+900) / 30000]
}


const Canvas = props => {

  const canvasRef = useRef(null)
  useEffect(() => {
    const canvas = canvasRef.current
    const c = canvas.getContext('2d');
    drawMap(c);
    // POINT
    data.point.map(p => {
      circle( c, ...calVal(p['@xPosition'], p['@yPosition']), p['@name'])
    });

    // ?Path

    data.path.map((path) => {
      const fName = get(path, "@sourcePoint");
      const tName = get(path, "@destinationPoint");
      console.log(fName, tName)

      const fPoint = data.point.find(i => i["@name"] == fName);
      const tPoint = data.point.find(i => i["@name"] == tName);

      drawArrow(c,
        ...calVal(fPoint['@xPosition'], fPoint['@yPosition']),
        ...calVal(tPoint['@xPosition'], tPoint['@yPosition']),
        2
      )
    })
    // 
  }, []);

  return <canvas
    style={{ border: '1px solid #ddd' }}
    width={SIZE.width}
    height={SIZE.height}
    ref={canvasRef}
    {...props}
  />
}

export default Canvas;

const unit = SIZE.width / 120;
const drawMap = (c) => {
  let i = 0;
  let x = 0;
  while (i <= 120) {
    drawLine(c, [i * unit, 0], [i * unit, SIZE.height]);
    i++;
  }


  while (x <= 80) {
    drawLine(c, [0, x * unit], [SIZE.width, x * unit]);
    x++;
  }



  circleNF(c, ...calVal(0, 0), '', 10, '#000');

  drawLine(c, [SIZE.width/2, 0], [SIZE.width/2, SIZE.height], 'red');
  drawLine(c, [0, SIZE.height/2], [SIZE.width, SIZE.height/2], 'red');



  // c.lineWidth = 6;
  // c.strokeStyle = "#333";
  // c.beginPath();
  // c.moveTo(100, 250);
  // c.bezierCurveTo(195, 189, 276, 318, 400, 250);
  // c.stroke();

  //   c.beginPath();
  //   canvas_arrow(c, 10, 30, 200, 150);
  //   canvas_arrow(c, 100, 200, 400, 50);
  //   canvas_arrow(c, 200, 30, 10, 150);
  //   canvas_arrow(c, 400, 200, 100, 50);
  //   c.stroke();

  // circle(c, 100, 100, 5)



  // var contextX = 0;
  // var contextY = 0;
  // var endPointX = 120;
  // var endPointY = 120;
  // var controlPointX = 35;
  // var controlPointY = 70;

  // drawCurvedArrow(c, contextX, contextY,
  //   endPointX, endPointY,
  //   controlPointX, controlPointY,
  //   3, // arrowWidth, try 30 for example !
  //   20, // width of the arrow head, try smaller values, 10...
  //   'blue');


  // drawArrow(c, 0, 0, 100, 100, 3, '#000')

}


const draw = ctx => {
  ctx.fillStyle = '#000000'
  ctx.beginPath()
  ctx.arc(50, 100, 20, 0, 2 * Math.PI)
  ctx.fill()
}

const drawLine = (c, from = [0, 0], to = [1, 2], bg = '#000') => {
  c.beginPath();
  c.moveTo(...from);
  c.lineTo(...to);
  c.lineWidth = 0.5;
  c.strokeStyle = bg;
  c.stroke();
  c.restore();
}

function canvas_arrow(context, fromx, fromy, tox, toy) {
  var headlen = 10; // length of head in pixels
  var dx = tox - fromx;
  var dy = toy - fromy;
  var angle = Math.atan2(dy, dx);
  context.moveTo(fromx, fromy);
  context.lineTo(tox, toy);
  context.lineTo(tox - headlen * Math.cos(angle - Math.PI / 6), toy - headlen * Math.sin(angle - Math.PI / 6));
  context.moveTo(tox, toy);
  context.lineTo(tox - headlen * Math.cos(angle + Math.PI / 6), toy - headlen * Math.sin(angle + Math.PI / 6));
}

function drawCurvedArrow(ctx, startPointX, startPointY,
  endPointX, endPointY,
  quadPointX, quadPointY,
  lineWidth,
  arrowWidth,
  color) {
  // GOOD PRACTICE: the function changes color and lineWidth -> save context!
  ctx.save();
  ctx.strokeStyle = color;
  ctx.lineWidth = lineWidth;

  // angle of the end tangeant, useful for drawing the arrow head
  var arrowAngle = Math.atan2(quadPointX - endPointX, quadPointY - endPointY) + Math.PI;

  // start a new path
  ctx.beginPath();
  ctx.moveTo(startPointX, startPointY);

  ctx.quadraticCurveTo(quadPointX, quadPointY, endPointX, endPointY);

  ctx.moveTo(endPointX - (arrowWidth * Math.sin(arrowAngle - Math.PI / 6)),
    endPointY - (arrowWidth * Math.cos(arrowAngle - Math.PI / 6)));

  ctx.lineTo(endPointX, endPointY);

  // ctx.bezierCurveTo(10, 10, 100, 318, endPointX, endPointY);

  ctx.lineTo(
    endPointX - (arrowWidth * Math.sin(arrowAngle + Math.PI / 6)),
    endPointY - (arrowWidth * Math.cos(arrowAngle + Math.PI / 6))
  );

  ctx.stroke();
  ctx.closePath();

  // GOOD PRACTICE -> restore the context as we saved it at the beginning
  // of the function
  ctx.restore();
}

function drawArrow(ctx, fromx, fromy, tox, toy, arrowWidth = 5, color = '#000') {
  //variables to be used when creating the arrow
  var headlen = 7;
  var angle = Math.atan2(toy - fromy, tox - fromx);

  ctx.save();
  ctx.strokeStyle = color;

  //starting path of the arrow from the start square to the end square
  //and drawing the stroke
  ctx.beginPath();
  ctx.moveTo(fromx, fromy);
  ctx.lineTo(tox, toy);
  ctx.lineWidth = 1;
  ctx.stroke();

  //starting a new path from the head of the arrow to one of the sides of
  //the point
  ctx.beginPath();
  ctx.moveTo(tox, toy);
  ctx.lineTo(tox - headlen * Math.cos(angle - Math.PI / 9),
    toy - headlen * Math.sin(angle - Math.PI / 9));

  //path from the side point of the arrow, to the other side point
  ctx.lineTo(tox - headlen * Math.cos(angle + Math.PI / 9),
    toy - headlen * Math.sin(angle + Math.PI / 9));

  //path from the side point back to the tip of the arrow, and then
  //again to the opposite side point
  ctx.lineTo(tox, toy);
  ctx.lineTo(tox - headlen * Math.cos(angle - Math.PI / 9),
    toy - headlen * Math.sin(angle - Math.PI / 9));

  ctx.lineWidth = arrowWidth;

  //draws the paths created above
  ctx.stroke();
  ctx.restore();
}


const circle = (ctx, x, y, text, r = 5) => {
  ctx.beginPath();
  ctx.strokeStyle = "#000";
  ctx.fillStyle = "red";

  ctx.arc(x, y, r, 0, 2 * Math.PI);
  ctx.fill();

  ctx.font = "7px Arial";
  ctx.strokeStyle = "#000";
  ctx.strokeText(text, x, y - 10);
  ctx.textAlign = "center";
  // ctx.strokeStyle = "red";

  ctx.stroke();
  ctx.restore();

}

const circleNF = (ctx, x, y, text, r = 5) => {
  ctx.beginPath();
  ctx.arc(x, y, r, 0, 2 * Math.PI);
  ctx.strokeStyle = "red";
  // ctx.fill();


  ctx.stroke();

}

// redraw 
// var canvas = document.getElementsByTagName("canvas")[0]; //get the canvas dom object
// var ctx = canvas.getContext("2d"); //get the context
// var c = {  //create an object to draw
//   x:0,  //x value
//   y:0,  //y value
//   r:5 //radius
// }
// var redraw = function(){
//   ctx.clearRect(0, 0, canvas.width, canvas.height); //clear canvas
//   ctx.beginPath();  //draw the object c
//   ctx.arc(c.x, c.y, c.r, 0, Math.PI*2); 
//   ctx.closePath();
//   ctx.fill();
    
//   requestAnimationFrame(redraw);
// }
// function move(){
//   var x = Math.random() // this returns a float between 0.0 and 1.0
//   c.x = x * canvas.width;
//   c.y = x * canvas.height;
// }
// redraw();

// setInterval(move, 1000);